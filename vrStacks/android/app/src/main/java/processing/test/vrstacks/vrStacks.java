package processing.test.vrstacks;

import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import processing.vr.*; 
import ketai.camera.*; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class vrStacks extends PApplet {




KetaiCamera cam;

public void setup() {
  ///VR DISPLAYY SETTINGS
  
  
  ///CAMERA DISPLAY SETTINGS
  orientation(LANDSCAPE);
  imageMode(CENTER);
  cam = new KetaiCamera(this, 1920, 1080, 24);
  cam.start();
  
  objList = new StringList();
  objListSorted = new StringList();

  bOjs = new ArrayList<boxObject>();
  bOjsSorted  = new ArrayList<boxObject>();
  
  getObjects();
  initializeObjects();
  
  generateEnviro();
 
}

public void draw() {
  
  background(0);
  lights();
  
  pushMatrix();
      eye();
      translate(0, 0,1000);
      box(10,10,10);
      tint(255, 200);
      image(cam,0,0);
  popMatrix();
  
  pushMatrix();
    translate(width/2, height/2);
    shape(cubes);
    //shape(grid);
    //drawObjects();
    //drawLabels();
  popMatrix();

}
public void onCameraPreviewEvent()
{
  cam.read();

}
boxObject bObj;
ArrayList<boxObject> bOjs;
ArrayList<boxObject> bOjsSorted;

float offSet;
boolean sortObj= false;
boolean drawLabels = false;
///////////////////////////////////////////////

class boxObject{
  
  int id;
  
  float x;
  float y;
  float z;
    
  String data;

  boxObject(int identity, String dataString){
    
    id = identity;
    
    data = dataString;
    String dataBits[] = split(data, ",");
    x = PApplet.parseFloat(dataBits[0]);
    y = PApplet.parseFloat(dataBits[1]);
    z = PApplet.parseFloat(dataBits[2]);
    
    if (y==0){ y=x;}
    if (z==0){z=y;}
       
  }
    
  public void displayBox(){
    noFill();
    strokeWeight(1);
    stroke(0);  
    pushMatrix();
      translate(0,0,offSet);
      box(x,y,z); 
    popMatrix();
  }
  
  public void displayLabels(){
    
    pushMatrix();    
    translate(0, -offSet);
      stroke(0);
      fill(0);
      textAlign(LEFT, CENTER);
      textSize(6);
      text(data, -width/2,0);
    popMatrix();
    
  }

}
////////////////////////////////////////////////////////

public void drawObjects(){
  
  for (int i=0; i< totalObjs; i++){
    boxObject b = bOjs.get(i);
    getOffset(i);
    b.displayBox();  
  }
  
  offSet=0;
  
}

public void drawLabels(){
  
  for (int i=0; i< totalObjs; i++){
    boxObject b = bOjs.get(i);
    getOffset(i);
    b.displayLabels();
  }
  
}

public void getOffset(int id){
  
  boxObject tempBox = bOjs.get(id);
  
  if (id==0){  
    offSet = tempBox.z/2;
  } else {
    boxObject tempBox1 = bOjs.get(id-1);
    offSet = offSet + (tempBox.z/2) + (tempBox1.z/2);
  }  
  
}

public void initializeObjects(){
  
  for (int i=totalObjs-1; i>=0; i--){
    
    String data = objList.get(i);
    bObj = new boxObject(i, data);
    bOjs.add(bObj);
    
  }
   
}

public void sortObjects(){
  
}
PShape grid;
PShape cubes;
///////////////////////////////////////////////

public void generateEnviro(){

  grid = createShape();
  grid.beginShape(LINES);
  grid.stroke(255);
  for (int x = -10000; x < +10000; x += 250) {
    grid.vertex(x, +1000, +10000);
    grid.vertex(x, +1000, -10000);
  }
  for (int z = -10000; z < +10000; z += 250) {
    grid.vertex(+10000, +1000, z);
    grid.vertex(-10000, +1000, z);      
  }  
  grid.endShape();  
  
  
  cubes = createShape(GROUP);
  for (int i = 0; i < 100; i++) {
    float x = random(-1000, +1000); 
    float y = random(-1000, +1000);
    float z = random(-1000, +1000);    
    float r = random(50, 150);
    PShape cube = createShape(BOX, r, r, r);
    cube.setStroke(false);
    cube.setFill(color(180));
    cube.translate(x, y, z);
    cubes.addChild(cube);
  }
  
}
String csvFile = "https://raw.githubusercontent.com/phiLangley/vrStacksData/master/data/headList.csv";
int totalObjs;
StringList objList;
StringList  objListSorted;
///////////////////////////////////////////////

public void getObjects(){
  
  ///LOAD OBJECTS FROM CSV
  String objects[] = loadStrings(csvFile);
  totalObjs = objects.length;

  ////CREATE LIST OF OBJECTS
  for (int i=0; i<objects.length; i++){
    objList.append(objects[i]); 
    objListSorted.append(objects[i]); 
  }
  
  objListSorted.sort();
  
  println(objList.get(0));

}
  public void settings() {  fullScreen(STEREO); }
}
