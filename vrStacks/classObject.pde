boxObject bObj;
ArrayList<boxObject> bOjs;
ArrayList<boxObject> bOjsSorted;

float offSet;
boolean sortObj= false;
boolean drawLabels = false;
///////////////////////////////////////////////

class boxObject{
  
  int id;
  
  float l;
  float w;
  float h; 
  
  float x;
  float y;
  float z;  
  
  float r;
  
  String data;
  
  PShape box;

  boxObject(int identity, String dataString){
    
    id = identity;
    
    data = dataString;
    String dataBits[] = split(data, ",");
    
    l = float(dataBits[0]) * 3;
    w = float(dataBits[1]) * 3;
    h = float(dataBits[2]) * 3;
    
    x = random(-1000, +1000); 
    y = random(-300, +300);
    z = random(-1000, +1000);
    
    r = PI/random(1,10);
            
    //box = createShape(BOX, l, w, h);
    //box.setStroke(color(255,0,0));
    //box.setFill(color(random(50,200)));
    //pushMatrix();
    //  box.translate(x, y, z);
    //  box.rotateY(PI/(random(1,3)));
    //popMatrix();

  }
  
  void updateBox(){
    
    x = x + random(-2,2);
    y = y + random(-2,2);
    z = z + random(-2,2);
    
    box = createShape(BOX, l, w, h);
    box.setStroke(false);
    box.setFill(color(180));
    
    pushMatrix();
      box.translate(x, y, z);
      r = r+(PI/60);
      box.rotateY(r);
    popMatrix();
    
  }
    
}
  
////////////////////////////////////////////////////////

void drawBoxes(){
  
  boxes = createShape(GROUP);
  for (int i=0; i< totalObjs; i++){
    boxObject b = bOjs.get(i);
    b.updateBox();
    boxes.addChild(b.box);
  }
  
  shape(boxes);
 
}


void initializeObjects(){
  
  for (int i=totalObjs-1; i>=0; i--){
    String data = objList.get(i);
    bObj = new boxObject(i, data);
    bOjs.add(bObj);
  }
   
}