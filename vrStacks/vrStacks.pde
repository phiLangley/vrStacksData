import processing.vr.*;
import ketai.camera.*;

KetaiCamera cam;

PShape boxes;

void setup() {
  //size(600,600,P3D);
  
  ///VR DISPLAYY SETTINGS
  fullScreen(STEREO);
  
  ///CAMERA DISPLAY SETTINGS
  orientation(LANDSCAPE);
  imageMode(CENTER);
  cam = new KetaiCamera(this, 4032, 3024, 60);
  cam.start();
  
  objList = new StringList();
  objListSorted = new StringList();

  bOjs = new ArrayList<boxObject>();
  bOjsSorted  = new ArrayList<boxObject>();
  
  getObjects();
  initializeObjects();
 
}

void draw() {
  
  background(255);
  lights();
  
  pushMatrix();
      eye();
      translate(0, 0,1000);
      tint(255, 200);
      image(cam,0,0);
  popMatrix();
  
  //pushMatrix();
    translate(width/2, height/2);
    drawBoxes();
  //popMatrix();

}